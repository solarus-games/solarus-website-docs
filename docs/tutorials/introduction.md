# Introduction

!!! info "Work in Progress"

    The Solarus tutorial text version is not ready yet.

    However, a complete **video tutorial** made by Christopho is available on YouTube, in two languages:

    * [English](https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufwkbfy49F_NrLtxCAiGJw5r)
    * [French](https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufzi8Qm27zkY_ncdKobEPDfV)

    Any help is welcome to transcribe these videos to text format.
