# Introduction

This tutorial will teach you how to create a Solarus _quest_, i.e. a game that is runnable by the Solarus engine. It will cover the basics of creating a quest, and will also cover some advanced topics.

For this, you will learn **how to use Solarus Quest Editor** and its integrated tools: map editor, sprite editor, etc. You'll also learn how to write **Lua scripts to control how your game logic works**.

!!! info "Coming soon"

    The text version is not ready yet. A complete **video tutorial** made by Christopho is available on YouTube:

    * [English](https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufwkbfy49F_NrLtxCAiGJw5r)
    * [French](https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufzi8Qm27zkY_ncdKobEPDfV)

    Any help is welcome to transcribe these videos to text format.
